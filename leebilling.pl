# copyright (c) 1982 - 2010, SirsiDynix.

# This is a copy of chargelist.pl that has been customized.  The control files
# behind this are jmtl.xxx.  The report name is "Lee Circ Bill Chargelist" and is
# run once a week on Friday morning, 9:30am.  Nelson and Lee
# scp dpne* srs@rigel.lib.byu.edu:/opt/sirsi/Unicorn/Rptsched/.
# dpne|leebilling|leebilling|n|201812061000|0000000000|DESKSUP||||||0|3||0||| (custom)
# Finally, change the ownership from DESKSUP to UNICOPS

BEGIN
  {
  chomp($script = `getpathname rptscript`);
  $runsubdir = "$script/Runsubs";
  unshift(@INC,$runsubdir);
  }

use set_var;
use new_initial;
use finalize_var;
use file_manipulate;
use LWP::UserAgent;
use LWP::Protocol::https;


setup_variables(\%Directives, \%TempFiles);
Initialize(\%Directives, \%TempFiles);
BuildOptions(\%Directives, \%TempFiles);	# for selcharge, seluser, [seluseracnt],
			      			# selitem, [selitemacnt], selcallnum,
			      			# selcatalog, [selcatacnt]
require("report_header.pl");

# BEGIN SUBROUTINES ADDED BY NELSON

# this subroutine was created by Moises
# checks to see if the record has an appeal pending
sub united {

    my $patronId = @_;
    chomp $patronId;
    my $pattern = ".NOTE.";
    $cmd = "echo $patronId | seluser -iB -oRB | veddump -a userxinfo -b USER_XINFO_BEGIN -e USER_XINFO_END -f USERX -o LDUSER -lUSER_ID 2>d.err > $TempFiles{'a'}";
    system($cmd);

    open(my $fh, '<:encoding(UTF-8)', $TempFiles{'a'})
        or die "Could not open file '$TempFiles{'a'}' $!";
    my $str = "";
    while(my $row = <$fh>) {
        $str = $str . $row;
    }
    my $offset = index($str,$pattern) + length($pattern);
    $str = substr($str, $offset);

    if ( index($str, ".COMMENT") != -1){
        $pattern = ".COMMENT";
        $offset = index($str, $pattern);
        $str = substr($str, 0, $offset);
    }
    else{
        $pattern = ".FINE";
        $offset = index($str, $pattern);
        $str = substr($str, 0, $offset);
    }
    $str = lc $str;

    # checks to see if user has an appeal pending
    if ( index($str, "appeal") != -1){
        return "Yes|";
    }
    else {
        return "No|";
    }
}

# this subroutine checks book prices for a given record at both Amazon and BookFinder
# If first needs to find an isbn. If no isbn is found it returns an error indicating so
# Additionally, it searches only new items on both Amazon and Bookfinder. However, the URL
# is included in the final spreadsheet, so if you are looking for a not new item, use the
# URL on Amazon or Bookfinder and modify the search. 
sub getBookPrices {

    my $ua = LWP::UserAgent->new;
    my $USA = "United States";

    my $catkey = shift;
    #my $green = $ua->get("https://search.lib.byu.edu/green/byu/record/lee.$ARGV[0]/green");
    # get necessary book info from byu library
    # argv[0] is the catkey of the book
    $cmd = "curl https://search.lib.byu.edu/green/byu/record/lee.$catkey/green > $TempFiles{'m'}";
    system($cmd);

    my $content = "";
    my $findhtml = "";
    open(my $fh, '<:encoding(UTF-8)', $TempFiles{'m'}) or die "Could not open file '$TempFiles{'m'}'";

    while (my $row = <$fh>) {
        chomp $row;
        $content = $content . $row;
    }
    close $fh;

    #system("rm $filename");
    my $index = index($content, "isbns");

    if($index < 0)
    {
        return "||Error, no isbns on record||||"; 
    }

    $content = substr($content, $index);
    $index = 12;

    $content = substr($content, $index);
    my $endIndex = index($content, "\"");
    my $isbn = substr($content, 0, $endIndex);

    # finished getting isbn. Now start searching in bookfinder
    ###############################
    my $bookfinderURL = "https://www.bookfinder.com/search/?author=&title=&lang=en&isbn=$isbn&new_used=*&destination=us&currency=USD&mode=basic&st=sr&ac=qr";
    $cmd = "curl \"https://www.bookfinder.com/search/?author=&title=&lang=en&isbn=$isbn&new_used=*&destination=us&currency=USD&mode=basic&st=sr&ac=qr\" > $TempFiles{'n'}";
    system($cmd);

    open(my $gh, '<:encoding(UTF-8)', $TempFiles{'n'}) or die "Could not open file '$TempFiles{'n'}";
    while (my $row = <$gh>) {
        chomp $row;
        $findhtml = $findhtml . $row;
    }
    close $gh;

    my $country = "";
    my $bfindex = index($findhtml, "results-explanatory-text-Logo");
    if($bfindex >= 0)
    {

        $bfindex = $bfindex + 1;
        $findhtml = substr($findhtml, $bfindex);
        $bfindex = index($findhtml, "results-explanatory-text-Logo");
        $bfindex = $bfindex + 1;
        $findhtml = substr($findhtml, $bfindex);
        $bfindex = index($findhtml, "results-explanatory-text-Logo");
        $bfindex = $bfindex + 31;
        $findhtml = substr($findhtml, $bfindex);
        $bfindex = index($findhtml, "</span");
        $country = substr($findhtml, 0, $bfindex);
    }

    if($country eq $USA)
    {
        # Make sure it is hardcover
        $bfcoverindex = $bfcoverindex + 11;
        my $coverhtml = substr($findhtml, $bfcoverindex);
        $bfcoverindex = index($coverhtml, ",");
        $coverhtml = substr($coverhtml, 0, $bfcoverindex);

        # Find price
        $bfindex = index($findhtml, "return overlib(");
        $findhtml = substr($findhtml, $bfindex);
        $bfindex = index($findhtml, "Book price");
        $findhtml = substr($findhtml, $bfindex);
        $bfindex = index($findhtml, ".");
        $bfindex = $bfindex - 2;
        $findhtml = substr($findhtml, $bfindex);
        $bfindex = index($findhtml, "\\");
        $findhtml = substr($findhtml, 0, $bfindex);

    }
    else
    {
        # Make sure it is hardcover
	my $bfcoverindex = index($findhtml, "item-note");
        $bfcoverindex = $bfcoverindex + 11;
        my $coverhtml = substr($findhtml, $bfcoverindex);
        $bfcoverindex = index($coverhtml, ",");
        $coverhtml = substr($coverhtml, 0, $bfcoverindex);

        # Find price 
	$bfindex = index($findhtml, "results-price");
        $findhtml = substr($findhtml, $bfindex);
        my $nindex = index($findhtml, "href");
        $findhtml = substr($findhtml, $nindex);
        $nindex = index($findhtml, ">\$");
        $findhtml = substr($findhtml, $nindex+2);
        $nindex = index($findhtml, "</");
        $findhtml = substr($findhtml, 0, $nindex);
    }
    ############################
    # start searching in amazon
    my $amazonURL = "https://www.amazon.com/gp/offer-listing/$isbn/ref=olp_f_new?ie=UTF8&f_all=true&f_new=true";
    my $response = $ua->get($amazonURL);

    my $html = $response->as_string();

    my $aindex = index($html, "a-size-large a-color-price");
    if($aindex < 0)
    {
        return "\$$findhtml|New|Amazon price not found.|Not Found|$bookfinderURL|$amazonURL|";
    }
    $aindex = $aindex + 55;
    $html  = substr($html, $aindex);
    $aindex = index($html, "</");
    $html = substr($html, 0, $aindex);

    # extract only necessary data from string
    #$html = substr($html, 16, 5);
    
    # set minimum threshold if below $5.00
    if($html < 5.00)
    {
        $html = 5.00;
    }
    if($findhtml < 5.00)
    {
        $findhtml = 5.00;
    }
    
    if(length($html) > 15 || $aindex < 0)
    {
        $html = "Amazon price not found.";
    }

    if(length($findhtml) > 15 || $bfindex < 0)
    {
        $findhtml = "Bookfinder price not found."
    }

    return "$findhtml|New|$html|New|$bookfinderURL|$amazonURL|";
}

sub getFloor {

    my ($homeLoc, $callnum) = @_;
    my $ua = LWP::UserAgent->new;

    # use this url to get the floor and section information
    my $json = $ua->get("http://ws.lib.byu.edu/locationcode/v1/getByLocationCode.php?locInfo[]=$homeLoc~$callnum");
    my $format = $json->as_string();

    my $area = "";
    my $index = index($format, "floor");
    $index = $index + 11;
    my $floor = substr($format, $index);
    $floor = substr($floor, 0, 1);
    
    # need to add check in case there is no floor or location
    # if the floor is 0 then it was not found, so there is no generalLocation
    if($floor ne "0") {
 
        my $areaIndex = index($format, "generalLocation");
        $areaIndex = $areaIndex + 18;
        $area = substr($format, $areaIndex);
        $area = substr($area, 0, 3);
        $area = substr($area, -1, 1);
    }

    my $r = $floor . "|" . $area; 
    return $r;
}

# Make the spreadsheet for the billing process for the librarians at the circulation desk.
sub makeSpreadsheet {

    my ($catkey, $patronId, $callNum, $barcode) = @_;
    $cmd = "echo \"$catkey\" | selitem -iC -ol > $TempFiles{'o'}";
    system($cmd);
    my $homeLocation = "";
    my $group = "";

    open(my $fh, '<:encoding(UTF-8)', $TempFiles{'o'}) or die "Could not open file '$TempFiles{'o'}'.";
    while (my $row = <$fh>) {
        chomp $row;
        $homeLocation = $row;
    }

    # gets the user group for the specified user
    $cmd = "echo \"$patronId\" | seluser -iB -opD > $TempFiles{'q'}";
    system($cmd);
    open(my $jh, '<:encoding(UTF-8)', $TempFiles{'q'}) or die "Could not open file '$TempFiles{'q'}'.";
    while (my $line = <$jh>) {
        chomp $line;
        $group = $line;
    }

    # remove trailing pipe character from these 2 variables   
    $homeLocation = substr($homeLocation, 0, length($homeLocation) - 1);
    $group = substr($group, 0, length($group) - 1);

    # uses sub routines to get more data for each entry
    my $floorSec = getFloor($homeLocation, $callNum);
    my $prices = getBookPrices($catkey);
    my $united = united($patronId);

    # fix prices variable if returns incorrectly
    if(length($prices) < 3) {
	$prices = "||||||";
    }

    $cmd = "echo \"$prices\" >> $TempFiles{'s'}";
    system($cmd);

    # remove the trailing pipe from the united variable
    $united = substr($united, 0, length($united) - 1);

    $cmd = "echo \"$catkey|$patronId|$group|$united|$floorSec|$prices\" | selitem -iC -oCSB | grep $barcode | selcallnum -iC -oCSA | selcatalog -iC -oCStye -e250 >> $TempFiles{'w'}";
    system($cmd);

    # rearrange the spreadsheet in the order that circulation wants it
    $cmd = "echo \"$catkey|$patronId|$group|$united|$floorSec|$prices\" | selitem -iC -oCSB | grep $barcode | selcallnum -iC -oCSA | selcatalog -iC -oCStye -e250 | awk -F\"|\" '{print \$2\"|\"\$3\"|\"\$4\"|\"\$14\"|\"\$16\"|\"\$15\"|\"\$6\"|\"\$7\"|\"\$17\"|\"\$18\"|\"\$5\"|\"\$10\"|\"\$11\"|\"\$13\"|\"\$8\"|\"\$9\"|\"\$12\"|\"\$1\"|\";}' >> $TempFiles{'y'}";
    system($cmd);
}

# this subroutine reads the origial chargelist.pl file, grabs the needed information,
# and uses it to make the leebilling spreadsheet
sub readFile {

    # add info to spreadsheet for each line in the file
    open(my $fh, '<:encoding(UTF-8)', $TempFiles{'x'}) or die "Could not open file '$TempFiles{'x'}'";
    
    while (my $row = <$fh>) {
        chomp $row;
        my @info = split(/\|/, $row);

	my $barcode = $info[1];
        my $catkey = $info[0];
        my $userId = $info[7];
	my $callNum = $info[8];
	
        makeSpreadsheet($catkey, $userId, $callNum, $barcode);
    }
}

# END SUBROUTINES ADDED BY NELSON, Main code below


 #The sort option messages
if ($Directives{'sortingdata0'} eq "title/author")
  {
# system("sirsiecho \"Sort: sorting by TITLE/AUTHOR\" >$TempFiles{'j'}");
  PrintMessage("\$(14099)","$TempFiles{'j'}");
  $sortopt = "+8 -9 +7 -8";
  }
elsif ($Directives{'sortingdata0'} eq "call number")
  {
# system("sirsiecho \"Sort: sorting by CALL NUMBER\" >$TempFiles{'j'}");
  PrintMessage("\$(14078)","$TempFiles{'j'}");
  $sortopt = "+6 -7 +8 -9";
  }
elsif ( $Directives{'sortingdata0'} eq "user name" )
  {
# system("sirsiecho \"Sort: sorting by USER NAME\" >$TempFiles{'j'}");
  PrintMessage("\$(14110)","$TempFiles{'j'}");
  $sortopt = "+4 -5 +8 -9";
  }
else    # userid
  {
# system("sirsiecho \"Sort: sorting by USER ID\" >$TempFiles{'j'}");
  PrintMessage("\$(14106)","$TempFiles{'j'}");
  $sortopt = "+5 -6 +8 -9";
  }

# Determine what the configuration is.
if ($Directives{'do_seluseracntdata0'} eq "N" ||
    $Directives{'seluseracntoptions'} eq "")
  {
  $cmd = "echo \"without user progs\n\" >> $TempFiles{'r'}";
  system($cmd);
  $user_progs = "seluser -iU -oUSAB $Directives{'seluseroptions'} 2>$TempFiles{'c'}";
  }
else
  {
  $cmd = "echo \"with user progs\n\" >> $TempFiles{'r'}";
  system($cmd);
  $progs = "seluseracnt -iU -oUS $Directives{'seluseracntoptions'} 2>$TempFiles{'d'}";
  $user_progs = "seluser -iU -oUSAB $Directives{'seluseroptions'} 2>$TempFiles{'c'} | $progs";
  }
$userstatus_progs = "seluserstatus -iU -oS $Directives{'seluserstatusoptions'} 2>$TempFiles{'l'}";

if ($Directives{'do_selitemacntdata0'} eq "N" ||
    $Directives{'selitemacntoptions'} eq "")
  {
  $cmd = "echo \"without item progs\n\" >> $TempFiles{'r'}";
  system($cmd);
  $item_progs = "selitem -iI -oNBS $Directives{'selitemoptions'} 2>$TempFiles{'e'}";
  }
else
  {
  $cmd = "echo \"with item progs\n\" >> $TempFiles{'r'}";
  system($cmd);
  $progs = "selitemacnt -iI -oS $Directives{'selitemacntoptions'} 2>$TempFiles{'f'}";
  $item_progs = "selitem -iI -oINBS $Directives{'selitemoptions'} 2>$TempFiles{'e'} | $progs";
  }

if ($Directives{'do_selcatacntdata0'} eq "N" ||
    $Directives{'selcatacntoptions'} eq "")
  {
  $cmd = "echo \"without cat progs\n\" >> $TempFiles{'r'}";
  system($cmd);
  $cat_progs = "selcatalog -iC -oCSAT $Directives{'selcatalogoptions'} 2>$TempFiles{'h'}";
  }
else
  {
  $cmd = "echo \"with cat progs\n\" >> $TempFiles{'r'}";
  system($cmd);
  $cat_progs = "selcatalog -iC -oCSAT $Directives{'selcatalogoptions'} 2>$TempFiles{'h'} | selcatacnt -iC -oS $Directives{'selcatacntoptions'} 2>$TempFiles{'i'}";
  }

# Run the required configuration.
my $charg_progs = "selcharge -oUIK $Directives{'selchargeoptions'} 2>$TempFiles{'b'}";
my $sel_progs = "selcallnum -iN -oCSA $Directives{'selcallnumoptions'} 2>$TempFiles{'g'}";


$cmd = "$charg_progs | $user_progs | $userstatus_progs | $item_progs | $sel_progs | $cat_progs > $TempFiles{'x'}";
system($cmd);

readFile();

# make the output file/spreadsheet
$cmd = "echo \"Patron ID|Patron Profile Group|Name|Item ID|Title|Call Number|Floor Number|Section|Publication Year|Edition|Appeal Pending?|Amazon Price|Amazon Book Condition|Amazon URL|Book Finder Price|Book Finder Book Condition|Book Finder URL|Catkey|\" >> $TempFiles{'z'}";
system($cmd);

$cmd = "cat $TempFiles{'y'} | sort | uniq >> $TempFiles{'z'}";
system($cmd);

# convert to xlsx format
$cmd = "cat $TempFiles{'z'} | butter -c\"|\" leebilling";
system($cmd);

# email the spreadsheet
$cmd = "mail -s \"LeeBilling Spreadsheet\" -a leebilling.xlsx -h10 lee_richards\@byu.edu";
system($cmd);

$Directives{'status'} = $?;

require("folddata.pl");
#finalize(\%Directives, \%TempFiles);
